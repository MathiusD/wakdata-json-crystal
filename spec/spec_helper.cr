require "spec"
require "json"
require "../src/wakdata/json"
require "../src/wakdata/json/config"
require "../src/wakdata/json/data"

struct SpecServerConfig
  include JSON::Serializable
  include JSON::Serializable::Strict

  @[JSON::Field(key: "test_server")]
  getter test_server : Wakdata::Json::Config::ServerConfig

  def initialize(@test_server : Wakdata::Json::Config::ServerConfig)
  end
end

struct SpecCDNConfig
  include JSON::Serializable
  include JSON::Serializable::Strict

  @[JSON::Field(key: "custom_cdn_config")]
  getter custom_cdn_config : Wakdata::Json::Config::CDNConfig

  def initialize(@custom_cdn_config : Wakdata::Json::Config::CDNConfig)
  end
end

SPEC_SERVER_CONFIG_FILE = File.open("./spec/resources/spec-server-config.json")

SPEC_SERVER_CONFIG = SpecServerConfig.from_json(SPEC_SERVER_CONFIG_FILE)

MOCKING_SERVER = SPEC_SERVER_CONFIG.test_server

FAKE_MOCK_SERVER = Wakdata::Json::Config::ServerConfig.new MOCKING_SERVER.protocol, MOCKING_SERVER.host, MOCKING_SERVER.port, "#{MOCKING_SERVER.path}/foo"

SPEC_CDN_CONFIG_FILE = File.open("./spec/resources/spec-cdn-config.json")

SPEC_CDN_CONFIG = SpecCDNConfig.from_json(SPEC_CDN_CONFIG_FILE)

CUSTOM_CDN_CONFIG = SPEC_CDN_CONFIG.custom_cdn_config

FAKE_CDN_CONFIG = Wakdata::Json::Config::CDNConfig.new Wakdata::Json::DEFAULT_CDN_CONFIG.config_file, [Wakdata::Json::Config::FileConfig.new "foo"]
