require "../spec_helper"

describe Wakdata::Json do
  it "config", tags: ["need-mocking-lib", "integration"] do
    Wakdata::Json.get_config.should be_a(Wakdata::Json::Data::Config)
  end

  it "version", tags: ["need-mocking-lib", "integration"] do
    Wakdata::Json.get_version.should match(/^[0-9]\.[0-9]+\.[0-9]+\.[0-9]+$/)
  end

  it "get_json", tags: ["need-mocking-lib", "integration"] do
    Wakdata::Json::DEFAULT_CDN_CONFIG.data_files.each do |data_file|
      Wakdata::Json.get_json(data_file.file_name).should be_a(JSON::Any)
    end
  end

  it "get_jsons", tags: ["need-mocking-lib", "integration"] do
    Wakdata::Json.get_jsons.should be_a(Hash(String, JSON::Any))
  end

  it "actions.get_jsons", tags: ["need-mocking-lib", "integration"] do
    Wakdata::Action.get_jsons.should be_a(Array(Wakdata::Action))
  end

  it "blueprints.get_jsons", tags: ["need-mocking-lib", "integration"] do
    Wakdata::Blueprint.get_jsons.should be_a(Array(Wakdata::Blueprint))
  end

  it "collectibleResources.get_jsons", tags: ["need-mocking-lib", "integration"] do
    Wakdata::CollectibleResource.get_jsons.should be_a(Array(Wakdata::CollectibleResource))
  end

  it "equipmentItemTypes.get_jsons", tags: ["need-mocking-lib", "integration"] do
    Wakdata::ItemType.get_jsons("equipmentItemTypes").should be_a(Array(Wakdata::ItemType))
  end

  it "harvestLoots.get_jsons", tags: ["need-mocking-lib", "integration"] do
    Wakdata::HarvestLoot.get_jsons.should be_a(Array(Wakdata::HarvestLoot))
  end

  it "itemProperties.get_jsons", tags: ["need-mocking-lib", "integration"] do
    Wakdata::ItemProperty.get_jsons.should be_a(Array(Wakdata::ItemProperty))
  end

  it "items.get_jsons", tags: ["need-mocking-lib", "integration"] do
    Wakdata::Item.get_jsons.should be_a(Array(Wakdata::Item))
  end

  it "itemTypes.get_jsons", tags: ["need-mocking-lib", "integration"] do
    Wakdata::ItemType.get_jsons.should be_a(Array(Wakdata::ItemType))
  end

  it "jobsItems.get_jsons", tags: ["need-mocking-lib", "integration"] do
    Wakdata::JobItem.get_jsons.should be_a(Array(Wakdata::JobItem))
  end

  it "recipeCategories.get_jsons", tags: ["need-mocking-lib", "integration"] do
    Wakdata::RecipeCategory.get_jsons.should be_a(Array(Wakdata::RecipeCategory))
  end

  it "recipeIngredients.get_jsons", tags: ["need-mocking-lib", "integration"] do
    Wakdata::RecipeIngredient.get_jsons.should be_a(Array(Wakdata::RecipeIngredient))
  end

  it "recipeResults.get_jsons", tags: ["need-mocking-lib", "integration"] do
    Wakdata::RecipeResult.get_jsons.should be_a(Array(Wakdata::RecipeResult))
  end

  it "recipes.get_jsons", tags: ["need-mocking-lib", "integration"] do
    Wakdata::Recipe.get_jsons.should be_a(Array(Wakdata::Recipe))
  end

  it "resources.get_jsons", tags: ["need-mocking-lib", "integration"] do
    Wakdata::Resource.get_jsons.should be_a(Array(Wakdata::Resource))
  end

  it "resourceTypes.get_jsons", tags: ["need-mocking-lib", "integration"] do
    Wakdata::ResourceType.get_jsons.should be_a(Array(Wakdata::ResourceType))
  end

  it "states.get_jsons", tags: ["need-mocking-lib", "integration"] do
    Wakdata::State.get_jsons.should be_a(Array(Wakdata::State))
  end

  it "config-local-success", tags: ["need-local_server"] do
    Wakdata::Json.get_config(MOCKING_SERVER).should eq(Wakdata::Json::Data::Config.new "0.0.0.0")
  end

  it "config-local-success-with-custom-cdn-config", tags: ["need-local_server"] do
    Wakdata::Json.get_config(MOCKING_SERVER, CUSTOM_CDN_CONFIG).should eq(Wakdata::Json::Data::Config.new "example")
  end

  it "config-local-missing-config", tags: ["need-local_server"] do
    expect_raises(Exception, "Config File (#{FAKE_MOCK_SERVER.url}/config.json) unreachable ! (Status code received : 404)") do
      Wakdata::Json.get_config FAKE_MOCK_SERVER
    end
  end

  it "version-local-success", tags: ["need-local_server"] do
    Wakdata::Json.get_version(MOCKING_SERVER).should eq("0.0.0.0")
  end

  it "version-local-success-with-custom-cdn-config", tags: ["need-local_server"] do
    Wakdata::Json.get_version(MOCKING_SERVER, CUSTOM_CDN_CONFIG).should eq("example")
  end

  it "version-local-config-failure", tags: ["need-local_server"] do
    expect_raises(Exception, "Config File (#{FAKE_MOCK_SERVER.url}/config.json) unreachable ! (Status code received : 404)") do
      Wakdata::Json.get_version FAKE_MOCK_SERVER
    end
  end

  it "get_json-local-success", tags: ["need-local_server"] do
    Wakdata::Json::DEFAULT_CDN_CONFIG.data_files.each do |data_file|
      Wakdata::Json.get_json(data_file.file_name, "0.0.0.0", MOCKING_SERVER).should eq([{"foo" => "bar"}])
    end
  end

  it "get_json-local-missing-file", tags: ["need-local_server"] do
    expect_raises(Exception, "File foo (#{FAKE_MOCK_SERVER.url}/0.0.0.0/foo.json) unreachable ! (Status code received : 404)") do
      Wakdata::Json.get_json("foo", "0.0.0.0", FAKE_MOCK_SERVER, FAKE_CDN_CONFIG)
    end
  end

  it "get_json-local-success-with-custom-cdn-config", tags: ["need-local_server"] do
    CUSTOM_CDN_CONFIG.data_files.each do |data_file|
      Wakdata::Json.get_json(data_file.file_name, "example", MOCKING_SERVER, CUSTOM_CDN_CONFIG).should eq([{"john" => "doe"}])
    end
  end

  it "get_jsons-local-success", tags: ["need-local_server"] do
    Wakdata::Json.get_jsons("0.0.0.0", MOCKING_SERVER).should eq({
      "actions"              => [{"foo" => "bar"}],
      "blueprints"           => [{"foo" => "bar"}],
      "collectibleResources" => [{"foo" => "bar"}],
      "equipmentItemTypes"   => [{"foo" => "bar"}],
      "harvestLoots"         => [{"foo" => "bar"}],
      "itemProperties"       => [{"foo" => "bar"}],
      "items"                => [{"foo" => "bar"}],
      "itemTypes"            => [{"foo" => "bar"}],
      "jobsItems"            => [{"foo" => "bar"}],
      "recipeCategories"     => [{"foo" => "bar"}],
      "recipeIngredients"    => [{"foo" => "bar"}],
      "recipeResults"        => [{"foo" => "bar"}],
      "recipes"              => [{"foo" => "bar"}],
      "resources"            => [{"foo" => "bar"}],
      "resourceTypes"        => [{"foo" => "bar"}],
      "states"               => [{"foo" => "bar"}],
    })
  end

  it "get_jsons-local-missing-files", tags: ["need-local_server"] do
    expect_raises(Exception, /^During fetch of Jsons, following errors are occured :(\n - File (.)+ \(#{FAKE_MOCK_SERVER.url}\/0\.0\.0\.0\/.+\..+\) unreachable ! \(Status code received : 404\)){#{Wakdata::Json::DEFAULT_CDN_CONFIG.data_files.size}}$/) do
      Wakdata::Json.get_jsons("0.0.0.0", FAKE_MOCK_SERVER)
    end
  end

  it "get_jsons-local-success-with-custom-cdn-config", tags: ["need-local_server"] do
    Wakdata::Json.get_jsons("example", MOCKING_SERVER, CUSTOM_CDN_CONFIG).should eq({
      "bar" => [{"john" => "doe"}],
    })
  end
end
