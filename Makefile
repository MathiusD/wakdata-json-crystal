default_target: fetch_data

clean:
	@rm -rf docs
	@rm -rf reports
	@rm -rf lib
	@rm -f shard.lock

install:
	@shards install

test_server:
	@python3 -m http.server --cgi --directory spec/resources/cdn 8080

test_server_docker_build:
	@docker build -t registry.gitlab.com/mathiusd/wakdata-json-crystal/test_server:local -f spec/resources/cdn/Dockerfile .

test_server_docker_run:
	@docker run -p 8080:80 registry.gitlab.com/mathiusd/wakdata-json-crystal/test_server:local

tests: regular_tests integration_tests

integration_tests:
	@crystal spec --tag integration --junit_output "reports/integration_tests-$(shell date '+%s').xml"

regular_tests:
	@crystal spec --tag ~integration --junit_output "reports/regular_tests-$(shell date '+%s').xml"
	
format:
	@crystal tool format --check

docs:
	@crystal docs lib/wakdata/src/wakdata.cr src/wakdata/json.cr