require "http/client"
require "json"
require "wakdata"
require "git-version-gen/src/git-version-gen.cr"

require "./json/config"
require "./json/data"

# Wakdata is module for fetch and use public data gived by Wakfu Team
module Wakdata::Json
  GitVersionGen.generate_version __DIR__

  # Basic ServerConfig of Wakfu CDN
  DEFAULT_SERVER_CONFIG = Wakdata::Json::Config::ServerConfig.new "https", "wakfu.cdn.ankama.com", "/gamedata"
  # Basic CDNConfig of Wakfu CDN
  DEFAULT_CDN_CONFIG = Wakdata::Json::Config::CDNConfig.new "config", [
    "actions",
    "blueprints",
    "collectibleResources",
    "equipmentItemTypes",
    "harvestLoots",
    "itemProperties",
    "itemTypes",
    "items",
    "jobsItems",
    "recipeCategories",
    "recipeIngredients",
    "recipeResults",
    "recipes",
    "resourceTypes",
    "resources",
    "states",
  ]

  # For get latest config of data available
  #
  # ```
  # require "wakdata"
  #
  # begin
  #   config = Wakdata.get_config
  #   puts "Latest config are : #{config}"
  # rescue exception
  #   puts "Error occured during fetch of config #{exception.message}"
  # end
  # ```
  def self.get_config(
    server_config : Wakdata::Json::Config::ServerConfig = DEFAULT_SERVER_CONFIG,
    cdn_config : Wakdata::Json::Config::CDNConfig = DEFAULT_CDN_CONFIG
  ) : Wakdata::Json::Data::Config
    config_url = "#{server_config.url}/#{cdn_config.config_file.path}"
    response = HTTP::Client.get config_url
    if response.status_code != 200
      raise "Config File (#{config_url}) unreachable ! (Status code received : #{response.status_code})"
    end
    return Wakdata::Json::Data::Config.from_json response.body
  end

  # For get latest version of data available
  #
  # ```
  # require "wakdata"
  #
  # begin
  #   version = Wakdata.get_version
  #   puts "Latest version are : #{version}"
  # rescue exception
  #   puts "Error occured during fetch of version #{exception.message}"
  # end
  # ```
  def self.get_version(
    server_config : Wakdata::Json::Config::ServerConfig = DEFAULT_SERVER_CONFIG,
    cdn_config : Wakdata::Json::Config::CDNConfig = DEFAULT_CDN_CONFIG
  ) : String
    configuration_data = get_config server_config, cdn_config
    return configuration_data.version
  end

  # For ensure version is set (Or is isn't get latest version available)
  #
  # ```
  # require "wakdata"
  #
  # def some_method(version : String?)
  #   version = Wakdata.ensure_version version
  #   # Here can use version without nil check
  # end
  # ```
  protected def self.ensure_version(
    version : String?,
    server_config : Wakdata::Json::Config::ServerConfig = DEFAULT_SERVER_CONFIG,
    cdn_config : Wakdata::Json::Config::CDNConfig = DEFAULT_CDN_CONFIG
  ) : String
    if version
      return version
    else
      return get_version server_config, cdn_config
    end
  end

  # For get specific json data
  #
  # ```
  # require "wakdata"
  #
  # items_data = Wakdata.get_json "items"
  # ```
  def self.get_json(
    name : String,
    version : String? = nil,
    server_config : Wakdata::Json::Config::ServerConfig = DEFAULT_SERVER_CONFIG,
    cdn_config : Wakdata::Json::Config::CDNConfig = DEFAULT_CDN_CONFIG
  ) : JSON::Any
    version = ensure_version version, server_config, cdn_config
    config_file = cdn_config.path_of?(name)
    raise "File #{name} unknown inside cdn_config specified" unless config_file

    json_url = "#{server_config.url}/#{version}/#{config_file.path}"
    response = HTTP::Client.get json_url
    if response.status_code != 200
      raise "File #{name} (#{json_url}) unreachable ! (Status code received : #{response.status_code})"
    end
    return JSON::Any.from_json response.body
  end

  # For get all jsons data
  #
  # ```
  # require "wakdata"
  #
  # data = Wakdata.get_jsons
  # ```
  def self.get_jsons(
    version : String? = nil,
    server_config : Wakdata::Json::Config::ServerConfig = DEFAULT_SERVER_CONFIG,
    cdn_config : Wakdata::Json::Config::CDNConfig = DEFAULT_CDN_CONFIG
  ) : Hash(String, JSON::Any)
    version = ensure_version version, server_config, cdn_config
    jsons = {} of String => JSON::Any
    exceptions = [] of Exception
    cdn_config.data_files.each do |data_file|
      begin
        jsons[data_file.file_name] = get_json data_file.file_name, version, server_config, cdn_config
      rescue exception
        exceptions << exception
      end
    end
    if exceptions.size > 0
      message = "During fetch of Jsons, following errors are occured :"
      exceptions.each do |exception|
        message = "#{message}\n - #{exception.message}"
      end
      raise message
    end
    return jsons
  end

  macro attach_get_jsons(struct_name, default_file_name)
    def {{struct_name}}.get_jsons(
      name : String = {{default_file_name}},
      version : String? = nil,
      server_config : Wakdata::Json::Config::ServerConfig = Wakdata::Json::DEFAULT_SERVER_CONFIG,
      cdn_config : Wakdata::Json::Config::CDNConfig = Wakdata::Json::DEFAULT_CDN_CONFIG
    ) : Array({{struct_name}})
      version = Wakdata::Json.ensure_version version, server_config, cdn_config
      config_file = cdn_config.path_of?(name)
      raise "File #{name} unknown inside cdn_config specified" unless config_file

      json_url = "#{server_config.url}/#{version}/#{config_file.path}"
      response = HTTP::Client.get json_url
      if response.status_code != 200
        raise "File #{name} (#{json_url}) unreachable ! (Status code received : #{response.status_code})"
      end
      return Array({{struct_name}}).from_json response.body
    end
  end
end

Wakdata::Json.attach_get_jsons Wakdata::Action, "actions"
Wakdata::Json.attach_get_jsons Wakdata::Blueprint, "blueprints"
Wakdata::Json.attach_get_jsons Wakdata::CollectibleResource, "collectibleResources"
Wakdata::Json.attach_get_jsons Wakdata::HarvestLoot, "harvestLoots"
Wakdata::Json.attach_get_jsons Wakdata::ItemProperty, "itemProperties"
Wakdata::Json.attach_get_jsons Wakdata::Item, "items"
Wakdata::Json.attach_get_jsons Wakdata::ItemType, "itemTypes"
Wakdata::Json.attach_get_jsons Wakdata::JobItem, "jobsItems"
Wakdata::Json.attach_get_jsons Wakdata::RecipeCategory, "recipeCategories"
Wakdata::Json.attach_get_jsons Wakdata::RecipeIngredient, "recipeIngredients"
Wakdata::Json.attach_get_jsons Wakdata::RecipeResult, "recipeResults"
Wakdata::Json.attach_get_jsons Wakdata::Recipe, "recipes"
Wakdata::Json.attach_get_jsons Wakdata::Resource, "resources"
Wakdata::Json.attach_get_jsons Wakdata::ResourceType, "resourceTypes"
Wakdata::Json.attach_get_jsons Wakdata::State, "states"
