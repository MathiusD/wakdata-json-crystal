require "json"

struct Wakdata::Json::Data::Config
  include JSON::Serializable
  include JSON::Serializable::Strict

  @[JSON::Field(key: "version")]
  getter version : String

  def initialize(@version : String)
  end
end
