require "json"

struct Wakdata::Json::Config::ServerConfig
  DEFAULT_PROTOCOL = "http"
  DEFAULT_PORTS    = {
    "http"  => 80,
    "https" => 443,
  }
  DEFAULT_PATH = nil

  include JSON::Serializable
  include JSON::Serializable::Strict

  @[JSON::Field(key: "protocol")]
  getter protocol : String?
  @[JSON::Field(key: "host")]
  getter host : String
  @[JSON::Field(key: "port")]
  getter port : Int32?
  @[JSON::Field(key: "path")]
  getter path : String?

  def initialize(@protocol : String?, @host : String, @port : Int32?, @path : String?)
  end

  def initialize(@protocol : String?, @host : String, @path : String?)
  end

  def url : String
    protocol_used : String
    if protocol
      protocol_used = protocol.not_nil!
    else
      protocol_used = DEFAULT_PROTOCOL
    end
    url = "#{protocol_used}://#{host}"
    if port && (!DEFAULT_PORTS.keys.includes?(protocol_used) || port != DEFAULT_PORTS[protocol_used])
      url += (":#{port.not_nil!}")
    end
    if path
      url += path.not_nil!
    end
    return url
  end
end

struct Wakdata::Json::Config::FileConfig
  DEFAULT_EXTENSION = "json"

  include JSON::Serializable
  include JSON::Serializable::Strict

  @[JSON::Field(key: "file_name")]
  getter file_name : String
  @[JSON::Field(key: "extension")]
  getter extension : String?

  def initialize(@file_name : String, @extension : String? = DEFAULT_EXTENSION)
  end

  def path : String
    path = "#{@file_name}"
    if @extension
      path += ".#{@extension}"
    end
    return path
  end
end

struct Wakdata::Json::Config::CDNConfig
  include JSON::Serializable
  include JSON::Serializable::Strict

  @[JSON::Field(key: "config_file")]
  getter config_file : FileConfig
  @[JSON::Field(key: "data_files")]
  getter data_files : Array(FileConfig)

  def initialize(@config_file : FileConfig, @data_files : Array(FileConfig))
  end

  def initialize(config_file_name : String, data_files_name : Array(String))
    @config_file = FileConfig.new config_file_name
    @data_files = [] of FileConfig
    data_files_name.each do |data_file_name|
      @data_files << FileConfig.new data_file_name
    end
  end

  def path_of?(name : String) : FileConfig?
    data_files.each do |data_file|
      if data_file.file_name == name
        return data_file
      end
    end
    return nil
  end
end
