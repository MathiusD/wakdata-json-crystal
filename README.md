# Wakdata::JSON - Crystal

[![Pipeline Status](https://gitlab.com/MathiusD/wakdata-json-crystal/badges/master/pipeline.svg)](https://gitlab.com/MathiusD/wakdata-json-crystal/-/pipelines)
[![Documentation](https://img.shields.io/badge/docs-available-brightgreen.svg)](https://mathiusd.gitlab.io/wakdata-json-crystal)

Wakdata::Json is crystal module for interact and fetch public data of MMORPG Wakfu from JSON files distributed in Wakfu CDN
